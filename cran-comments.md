
Release Summary


This is a patch release to fix checks on old releases. 

## Test environments
* local Windows 10 1903 x86_64, R 4.0.0 

* ubuntu 16.04.6 (on Travis), R 3.5.3

* win-builder (devel, release)

* Mac OSX Github actions, R 4.0.0

## R CMD check results

0 errors | 0 warnings | 0 note

revdepcheck results

There are currently no reverse dependencies.

